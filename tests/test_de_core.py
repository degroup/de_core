""" unit tests for de.core portion """
# import os - imported via de.core.*
from unittest.mock import patch

import pytest
import shutil

from de.core import *


@pytest.fixture
def ext_ns_file():
    """ prepare NAMESPACE_EXTEND_ENTRY_POINT and remove after test """
    mod_name, func_name = NAMESPACE_EXTEND_ENTRY_POINT.split(':')
    hook_file = os.path.join(TESTS_FOLDER, mod_name + PY_EXT)
    assert not os.path.exists(hook_file), f"TEST RUN PREVENTED TO OVERWRITE {hook_file}"
    file_handle = open(hook_file, 'w')
    file_handle.write(f"""def {func_name}(nev):\n    """)

    yield file_handle

    os.remove(hook_file)


RETURN_CODE = 123456789
STDOUT_LINE = b'stdout'
STDERR_LINE = b'stderr'


def subprocess_run_return(*_args, **_kwargs):
    """ mock to simulate subprocess.run return object. """
    class _Return:
        returncode = RETURN_CODE
        stdout = STDOUT_LINE
        stderr = STDERR_LINE
    return _Return()


class TestFileVersionHelpers:
    """ test bump_file_version() and code_file_version functions """
    def test_bump_file_version_invalid_file(self):
        err = bump_file_version('::invalid_file_name::')
        assert err

    def test_bump_file_version_empty_file(self):
        tst_file = 'test_bump_version.py'
        try:
            with open(tst_file, 'w') as file_handle:
                file_handle.write("")
            err = bump_file_version(tst_file)
            assert err
        finally:
            if os.path.exists(tst_file):
                os.remove(tst_file)

    def test_bump_file_version_multi_version(self):
        tst_file = 'test_bump_version.py'
        try:
            with open(tst_file, 'w') as file_handle:
                file_handle.write("__version__ = '1.2.3'\n__version__ = '2.3.4'")
            err = bump_file_version(tst_file)
            assert err
        finally:
            if os.path.exists(tst_file):
                os.remove(tst_file)

    def test_bump_file_version_major(self):
        tst_file = 'test_bump_version.py'
        try:
            with open(tst_file, 'w') as file_handle:
                file_handle.write("__version__ = '1.2.3'\n")

            err = bump_file_version(tst_file, version_part=1)

            assert not err

            content = file_content(tst_file)
            assert "__version__ = '1.2.3'" not in content
            assert "__version__ = '2.2.3'" in content
        finally:
            if os.path.exists(tst_file):
                os.remove(tst_file)

    def test_bump_file_version_minor(self):
        tst_file = 'test_bump_version.py'
        try:
            with open(tst_file, 'w') as file_handle:
                file_handle.write("__version__ = '1.2.3'\n\nversion = '2.3.4'")

            err = bump_file_version(tst_file, version_part=2)

            assert not err

            content = file_content(tst_file)
            assert "__version__ = '1.2.3'" not in content
            assert "__version__ = '1.3.3'" in content
        finally:
            if os.path.exists(tst_file):
                os.remove(tst_file)

    def test_bump_file_version_build(self):
        tst_file = 'test_bump_version.py'
        try:
            with open(tst_file, 'w') as file_handle:
                file_handle.write("__version__ = '1.2.3'\nversion = '2.3.4'")

            err = bump_file_version(tst_file)

            assert not err

            content = file_content(tst_file)
            assert "__version__ = '1.2.3'" not in content
            assert "__version__ = '1.2.4'" in content
        finally:
            if os.path.exists(tst_file):
                os.remove(tst_file)

    def test_bump_file_version_remove_suffix(self):
        tst_file = 'test_bump_version.py'
        try:
            with open(tst_file, 'w') as file_handle:
                file_handle.write("__version__ = '1.2.3pre'\nversion = '2.3.4'")

            err = bump_file_version(tst_file, version_part=2)

            assert not err

            content = file_content(tst_file)
            assert "__version__ = '1.2.3'" not in content
            assert "__version__ = '1.3.3'" in content
        finally:
            if os.path.exists(tst_file):
                os.remove(tst_file)

    def test_bump_file_version_keeping_comment(self):
        tst_file = 'test_bump_version.py'
        comment_str = "  # comment string"
        try:
            with open(tst_file, 'w') as file_handle:
                file_handle.write(f"__version__ = '1.2.3'{comment_str}\nversion = '2.3.4'")

            err = bump_file_version(tst_file, version_part=1)

            assert not err

            content = file_content(tst_file)
            assert f"__version__ = '2.2.3'{comment_str}" in content
        finally:
            if os.path.exists(tst_file):
                os.remove(tst_file)

    def test_code_file_version(self):
        tst_file = 'test_code_version.py'
        version_str = '33.22.111pre'
        try:
            with open(tst_file, 'w') as file_handle:
                file_handle.write(f"__version__ = '{version_str}'  # comment\nversion = '9.6.3'")
            assert code_file_version(tst_file) == version_str
        finally:
            if os.path.exists(tst_file):
                os.remove(tst_file)

    def test_code_file_version_invalid_file_content(self):
        tst_file = 'test_code_version.py'
        try:
            with open(tst_file, 'w') as file_handle:
                file_handle.write("")                       # empty file
            assert not code_file_version(tst_file)

            with open(tst_file, 'w') as file_handle:
                file_handle.write("version__ = '1.2.3'")    # invalid version var prefix
            assert not code_file_version(tst_file)
        finally:
            if os.path.exists(tst_file):
                os.remove(tst_file)

    def test_code_file_version_invalid_file_name(self):
        assert not code_file_version('::invalid_file_name::')


class TestConsoleExecute:
    @patch.object(subprocess, 'run', autospec=True)
    def test_sh_exec_args(self, mock_method):
        cmd_line = "cmd arg1 arg2"
        extra_args = ['extra_arg1', 'extra_arg2']

        sh_exec(cmd_line, extra_args)
        mock_method.assert_called_with(
            cmd_line.split(" ") + extra_args, stdout=None, stderr=None, input=b'', check=True)

        sh_exec(cmd_line, extra_args, console_input='con_inp')
        mock_method.assert_called_with(
            cmd_line.split(" ") + extra_args, stdout=None, stderr=None, input=b'con_inp', check=True)

        sh_exec(cmd_line, extra_args, lines_output=list())
        mock_method.assert_called_with(
            cmd_line.split(" ") + extra_args, stdout=subprocess.PIPE, stderr=subprocess.PIPE, input=b'', check=True)

        sh_exec(cmd_line, extra_args, console_input='con_inp', lines_output=list())
        mock_method.assert_called_with(
            cmd_line.split(" ") + extra_args, stdout=subprocess.PIPE, stderr=subprocess.PIPE, input=b'con_inp',
            check=True)

    @patch.object(subprocess, 'run', new=subprocess_run_return)
    def test_sh_exec_run_returned_values(self):
        cmd_line = "cmd arg1 arg2"
        extra_args = ['extra_arg1', 'extra_arg2']
        lines_output = list()

        assert sh_exec(cmd_line, extra_args, lines_output=lines_output) == RETURN_CODE
        assert STDOUT_LINE.decode() in lines_output
        assert "STDERR=" + STDERR_LINE.decode() in lines_output

    @patch.object(subprocess, 'run', new_callable=subprocess_run_return)
    def test_sh_exec_run_exception(self, _return_obj):
        cmd_line = "cmd arg1 arg2"
        extra_args = ['extra_arg1', 'extra_arg2']
        lines_output = list()
        assert sh_exec(cmd_line, extra_args, lines_output=lines_output) == 999     # _Return() is not callable exc


class TestNamespaceEnvVars:
    """ test namespace_env_vars() function """
    def test_app_env(self):
        file_name = os.path.join(TESTS_FOLDER, 'buildozer.spec')
        try:
            with open(file_name, 'w') as tst_conf:
                tst_conf.write("spec")
            nev = namespace_env_vars(TESTS_FOLDER, TESTS_FOLDER)
            assert nev['project_type'] == APP_PRJ
        finally:
            if os.path.exists(file_name):
                os.remove(file_name)

    def test_de_env(self):
        namespace = 'de'
        module = 'core'
        nev = namespace_env_vars(namespace)
        assert isinstance(nev, dict)
        cur_dir = os.getcwd()
        assert nev['namespace_name'] == namespace
        assert nev['NAMESPACE_EXTEND_ENTRY_POINT'] == 'setup_hooks:extend_namespace_env_vars'
        assert nev['NAMESPACE_EXTEND_ENTRY_POINT'] == NAMESPACE_EXTEND_ENTRY_POINT
        assert nev['PORTIONS_COMMON_DIR'] == PORTIONS_COMMON_DIR == 'portions_common_root'
        assert nev['REQ_FILE_NAME'] == REQ_FILE_NAME == 'requirements.txt'
        assert nev['REQ_TEST_FILE_NAME'] == REQ_TEST_FILE_NAME == 'test_requirements.txt'
        assert nev['TEMPLATE_FILE_NAME_PREFIX'] == TEMPLATE_FILE_NAME_PREFIX == 'de_tpl_'
        assert nev['TEMPLATE_PLACEHOLDER_ID_PREFIX'] == TEMPLATE_PLACEHOLDER_ID_PREFIX == '# '
        assert nev['TEMPLATE_PLACEHOLDER_ID_SUFFIX'] == TEMPLATE_PLACEHOLDER_ID_SUFFIX == '#('
        assert nev['TEMPLATE_PLACEHOLDER_ARGS_SUFFIX'] == TEMPLATE_PLACEHOLDER_ARGS_SUFFIX == ')#'
        assert nev['TEMPLATE_INCLUDE_FILE_PLACEHOLDER_ID'] == TEMPLATE_INCLUDE_FILE_PLACEHOLDER_ID == 'IncludeFile'
        assert nev['root_path'] == cur_dir
        assert nev['setup_path'] == cur_dir
        assert nev['setup_file'] == os.path.join(cur_dir, 'setup' + PY_EXT)
        assert nev['project_type'] == 'module' == MODULE_PRJ
        assert nev['portion_name'] == module
        assert nev['portion_file_name'] == module + PY_EXT
        assert nev['portion_file_path'] == os.path.join(cur_dir, namespace, module + PY_EXT)
        assert nev['package_name'] == namespace + '_' + module
        assert nev['pip_name'] == namespace + '-' + module
        assert nev['import_name'] == namespace + '.' + module
        assert nev['package_version'] == code_file_version(os.path.join(cur_dir, namespace, module + PY_EXT))
        assert nev['repo_root']
        assert nev['repo_pages']
        assert nev['pypi_root']
        assert nev['portion_pypi_root']

        assert len(nev['docs_require']) == 0
        assert len(nev['install_require']) == 1
        assert nev['install_require'] == ['de_core']
        assert len(nev['setup_require']) == 0
        assert len(nev['tests_require']) == 11
        assert nev['portions_package_names'] == ['de_core']
        assert nev['find_packages_include'] == ['de']
        assert nev['package_resources'] == []

    def test_non_existent_env(self):
        namespace = 'xY'
        # module = '{portion-name}'
        nev = namespace_env_vars(namespace, root_path=TESTS_FOLDER)
        assert isinstance(nev, dict)
        cur_dir = os.getcwd()
        assert nev['namespace_name'] == namespace
        assert nev['NAMESPACE_EXTEND_ENTRY_POINT'] == 'setup_hooks:extend_namespace_env_vars'
        assert nev['NAMESPACE_EXTEND_ENTRY_POINT'] == NAMESPACE_EXTEND_ENTRY_POINT
        assert nev['PORTIONS_COMMON_DIR'] == PORTIONS_COMMON_DIR == 'portions_common_root'
        assert nev['REQ_FILE_NAME'] == REQ_FILE_NAME == 'requirements.txt'
        assert nev['REQ_TEST_FILE_NAME'] == REQ_TEST_FILE_NAME == 'test_requirements.txt'
        assert nev['TEMPLATE_FILE_NAME_PREFIX'] == TEMPLATE_FILE_NAME_PREFIX == 'de_tpl_'
        assert nev['TEMPLATE_PLACEHOLDER_ID_PREFIX'] == TEMPLATE_PLACEHOLDER_ID_PREFIX == '# '
        assert nev['TEMPLATE_PLACEHOLDER_ID_SUFFIX'] == TEMPLATE_PLACEHOLDER_ID_SUFFIX == '#('
        assert nev['TEMPLATE_PLACEHOLDER_ARGS_SUFFIX'] == TEMPLATE_PLACEHOLDER_ARGS_SUFFIX == ')#'
        assert nev['TEMPLATE_INCLUDE_FILE_PLACEHOLDER_ID'] == TEMPLATE_INCLUDE_FILE_PLACEHOLDER_ID == 'IncludeFile'
        assert nev['root_path'] == os.path.join(cur_dir, TESTS_FOLDER)
        assert nev['setup_path'] == os.path.join(cur_dir, TESTS_FOLDER)
        assert 'setup_file' not in nev
        assert nev['project_type'] == ''
        assert 'package_version' not in nev
        # assert nev['repo_root']
        # assert nev['repo_pages']
        # assert nev['pypi_root']
        # assert nev['portion_pypi_root']

        assert len(nev['docs_require']) == 0
        assert len(nev['install_require']) == 1
        assert nev['install_require'] == ['de_core']
        assert nev['setup_require'] == ['de_core']
        assert len(nev['tests_require']) == 0
        assert len(nev['portions_package_names']) == 0
        assert 'find_packages_include' not in nev
        assert 'package_resources' not in nev

    def test_invalid_env_doesnt_raise(self):
        name = 'invalid_env_id'
        nev = namespace_env_vars(name, root_path="invalid_root_path")
        assert nev['namespace_name'] == name

    def test_add_env(self, ext_ns_file):
        test_env_key = 'test_env_key'
        test_env_val = 'test_env_val'
        ext_ns_file.write(f"""nev['{test_env_key}'] = '{test_env_val}'\n""")
        ext_ns_file.close()

        nev = namespace_env_vars('xy', TESTS_FOLDER)

        if nev_update_hook(dict(), TESTS_FOLDER):   # skip tests directly after upgrade of ae.core and not yet deployed
            assert test_env_key in nev
            assert nev[test_env_key] == test_env_val

    def test_change_env(self, ext_ns_file):
        test_env_val = 'test_env_val'
        ext_ns_file.write(f"""nev['install_require'].append('{test_env_val}')\n""")
        ext_ns_file.close()

        nev = namespace_env_vars('xy', TESTS_FOLDER)

        install_req_len = len(nev['install_require'])
        assert install_req_len == 2
        assert 'de_core' in nev['install_require']
        assert test_env_val in nev['install_require']
        if nev_update_hook(nev, TESTS_FOLDER):     # skip tests directly after upgrade of ae.core and not yet deployed
            assert len(nev['install_require']) == install_req_len + 1
            assert nev['install_require'] == ['de_core', test_env_val, test_env_val]

    def test_change_env_double(self, ext_ns_file):
        test_val = 'test_env_val'
        redirected_hook = NAMESPACE_EXTEND_ENTRY_POINT + '2'
        ext_ns_file.write(f"""nev['NAMESPACE_EXTEND_ENTRY_POINT'] = '{redirected_hook}'""")
        ext_ns_file.write(f"""\n\n""")
        ext_ns_file.write(f"""def extend_namespace_env_vars2(nev):\n    nev['install_require'].append('{test_val}')""")
        ext_ns_file.close()

        nev = namespace_env_vars('xy', TESTS_FOLDER)

        install_req_len = len(nev['install_require'])
        assert install_req_len == 2
        assert 'de_core' in nev['install_require']
        assert test_val in nev['install_require']

    def test_namespace_root_in_docs(self):
        file_name = os.path.join(TESTS_FOLDER, 'conf.py')
        try:
            with open(file_name, 'w') as tst_conf:
                tst_conf.write("file-content-irrelevant")
            nev = namespace_env_vars('de', TESTS_FOLDER)
            assert nev['setup_path'].endswith('..')
        finally:
            if os.path.exists(file_name):
                os.remove(file_name)

    def test_namespace_root_setup_version(self):
        file_name = os.path.join(TESTS_FOLDER, 'setup.py')
        root_version = "12.33.444"
        try:
            with open(file_name, 'w') as tst_setup:
                tst_setup.write(f"__version__ = '{root_version}'")
            nev = namespace_env_vars(root_path=TESTS_FOLDER)
            assert nev['root_version'] == root_version
        finally:
            if os.path.exists(file_name):
                os.remove(file_name)

    def test_namespace_root_portion_modules(self):
        namespace = 'xyz'
        root_path = os.path.join(TESTS_FOLDER, namespace)
        root_setup_file_name = os.path.join(root_path, 'setup.py')
        req_file_name = os.path.join(root_path, 'requirements.txt')
        portion_name = 'tst_pkg'
        package_dir = os.path.join(TESTS_FOLDER, namespace + "_" + portion_name)
        package_path = os.path.join(package_dir, namespace, portion_name)
        module_name = "tst_module_name.py"
        try:
            os.makedirs(root_path)
            with open(root_setup_file_name, 'w') as tst_setup:
                tst_setup.write(f"__version__ = '0.2.4'")
            with open(req_file_name, 'w') as req:
                req.write(namespace + '_' + portion_name)
            os.makedirs(package_path)
            with open(os.path.join(package_path, '__init__.py'), 'w') as tst_mod:
                tst_mod.write("any_content = ''")
            with open(os.path.join(package_path, module_name), 'w') as tst_mod:
                tst_mod.write("any_content = ''")

            nev = namespace_env_vars(namespace, root_path=root_path)
            assert f"{namespace}.{portion_name}.{os.path.splitext(module_name)[0]}" in nev['portions_import_names']
        finally:
            if os.path.exists(package_dir):
                shutil.rmtree(package_dir)
            if os.path.exists(root_path):
                shutil.rmtree(root_path)

    def test_namespace_extra_modules(self):
        namespace = 'xxx'
        portion_name = 'tst_pkg'
        package_dir = os.path.join(TESTS_FOLDER, namespace + "_" + portion_name)
        req_file_name = os.path.join(package_dir, 'requirements.txt')
        package_path = os.path.join(package_dir, namespace, portion_name)
        module_name = "tst_module_name.py"
        try:
            os.makedirs(package_path)
            with open(req_file_name, 'w') as req:
                req.write(namespace + '_' + portion_name)
            with open(os.path.join(package_path, '__init__.py'), 'w') as tst_mod:
                tst_mod.write("any_content = ''")
            with open(os.path.join(package_path, module_name), 'w') as tst_mod:
                tst_mod.write("any_content = ''")

            nev = namespace_env_vars(namespace, package_dir)

            assert os.path.splitext(module_name)[0] in nev['portion_modules']
            assert '__init__' not in nev['portion_modules']
        finally:
            if os.path.exists(package_dir):
                shutil.rmtree(package_dir)
            # if os.path.exists(req_file_name):
            #     os.remove(req_file_name)


class TestPatchTemplates:
    """ test patch_templates() function """
    def test_call(self):
        assert patch_templates(dict()) == list()

    def test_file_include_content(self):
        include_file_name = "tests/inc.tst.file"
        fn = "file_include_content.txt"
        patched_file_name = f"tests/{fn}"
        tpl_file_name = f"tests/{TEMPLATE_FILE_NAME_PREFIX}{fn}"
        include_file_content = "replacement string"
        try:
            with open(include_file_name, 'w') as file_handle:
                file_handle.write(include_file_content)

            tpl = f"{TEMPLATE_PLACEHOLDER_ID_PREFIX}{TEMPLATE_INCLUDE_FILE_PLACEHOLDER_ID}"
            tpl += f"{TEMPLATE_PLACEHOLDER_ID_SUFFIX}{include_file_name}{TEMPLATE_PLACEHOLDER_ARGS_SUFFIX}"
            with open(tpl_file_name, 'w') as file_handle:
                file_handle.write(tpl)

            assert patch_templates(dict(), TESTS_FOLDER) == list()

            patched = patch_templates(dict())
            assert patched == [tpl_file_name]

            content = file_content(patched_file_name)
            assert include_file_content in content
            assert TEMPLATE_PLACEHOLDER_ID_PREFIX not in content
            assert TEMPLATE_INCLUDE_FILE_PLACEHOLDER_ID not in content
            assert TEMPLATE_PLACEHOLDER_ID_SUFFIX not in content
            assert TEMPLATE_PLACEHOLDER_ARGS_SUFFIX not in content
        finally:
            if os.path.exists(include_file_name):
                os.remove(include_file_name)
            if os.path.exists(tpl_file_name):
                os.remove(tpl_file_name)
            if os.path.exists(patched_file_name):
                os.remove(patched_file_name)

    def test_file_include_with_nev_vars(self):
        include_file_name = "tests/inc.tst.file"
        fn = "file_include_content.txt"
        patched_file_name = f"tests/{fn}"
        tpl_file_name = f"tests/{TEMPLATE_FILE_NAME_PREFIX}{fn}"
        try:
            tpl = "{TEMPLATE_PLACEHOLDER_ID_PREFIX}{TEMPLATE_INCLUDE_FILE_PLACEHOLDER_ID}"
            tpl += "{TEMPLATE_PLACEHOLDER_ID_SUFFIX}"
            tpl += f"{include_file_name}"
            tpl += "{TEMPLATE_PLACEHOLDER_ARGS_SUFFIX}"
            with open(tpl_file_name, 'w') as file_handle:
                file_handle.write(tpl)

            nev = namespace_env_vars('de')
            patched = patch_templates(nev)
            assert patched == [tpl_file_name]

            content = file_content(patched_file_name)
            assert TEMPLATE_PLACEHOLDER_ID_PREFIX not in content
            assert "TEMPLATE_PLACEHOLDER_ID_PREFIX" not in content
            assert TEMPLATE_INCLUDE_FILE_PLACEHOLDER_ID not in content
            assert "TEMPLATE_INCLUDE_FILE_PLACEHOLDER_ID" not in content
            assert TEMPLATE_PLACEHOLDER_ID_SUFFIX not in content
            assert "TEMPLATE_PLACEHOLDER_ID_SUFFIX" not in content
            assert TEMPLATE_PLACEHOLDER_ARGS_SUFFIX not in content
            assert "TEMPLATE_PLACEHOLDER_ARGS_SUFFIX" not in content
        finally:
            if os.path.exists(tpl_file_name):
                os.remove(tpl_file_name)
            if os.path.exists(patched_file_name):
                os.remove(patched_file_name)

    def test_file_include_default_and_with_nev_vars(self):
        include_file_name = "tests/inc.tst.file"
        fn = "file_include_content.txt"
        patched_file_name = f"tests/{fn}"
        tpl_file_name = f"tests/{TEMPLATE_FILE_NAME_PREFIX}{fn}"
        default = "default string"
        try:
            tpl = "{TEMPLATE_PLACEHOLDER_ID_PREFIX}{TEMPLATE_INCLUDE_FILE_PLACEHOLDER_ID}"
            tpl += "{TEMPLATE_PLACEHOLDER_ID_SUFFIX}"
            tpl += f"{include_file_name},{default}"
            tpl += "{TEMPLATE_PLACEHOLDER_ARGS_SUFFIX}"
            with open(tpl_file_name, 'w') as file_handle:
                file_handle.write(tpl)

            nev = namespace_env_vars('de')
            patched = patch_templates(nev)
            assert patched == [tpl_file_name]

            content = file_content(patched_file_name)
            assert default in content
            assert TEMPLATE_PLACEHOLDER_ID_PREFIX not in content
            assert "TEMPLATE_PLACEHOLDER_ID_PREFIX" not in content
            assert TEMPLATE_INCLUDE_FILE_PLACEHOLDER_ID not in content
            assert "TEMPLATE_INCLUDE_FILE_PLACEHOLDER_ID" not in content
            assert TEMPLATE_PLACEHOLDER_ID_SUFFIX not in content
            assert "TEMPLATE_PLACEHOLDER_ID_SUFFIX" not in content
            assert TEMPLATE_PLACEHOLDER_ARGS_SUFFIX not in content
            assert "TEMPLATE_PLACEHOLDER_ARGS_SUFFIX" not in content
        finally:
            if os.path.exists(tpl_file_name):
                os.remove(tpl_file_name)
            if os.path.exists(patched_file_name):
                os.remove(patched_file_name)


class TestPortionData:
    """ portion_type_name_modules unit tests """
    def test_find_package_data_img(self):
        namespace_name = TESTS_FOLDER
        portion_name = 'test_pkg_with_resources'
        root_path = namespace_name
        pkg_path = os.path.join(root_path, portion_name)
        tst_pgk_file = os.path.join(pkg_path, '__init__.py')
        res_path1 = os.path.join(pkg_path, 'img')
        res_file1 = os.path.join(res_path1, 'res.ext')
        res_path2 = os.path.join(res_path1, 'res_deep')
        res_file2 = os.path.join(res_path2, 'res2.ext')
        res_file3 = os.path.join(pkg_path, 'widget.kv')
        try:
            os.makedirs(res_path2)
            with open(tst_pgk_file, 'w') as file_handle:
                file_handle.write("v = 3")
            with open(res_file1, 'w') as file_handle:
                file_handle.write("some resource content")
            with open(res_file2, 'w') as file_handle:
                file_handle.write("res content2")
            with open(res_file3, 'w') as file_handle:
                file_handle.write("kv language content")

            files = find_package_data(namespace_name, portion_name)
            assert len(files) == 3
            assert files[0] == os.path.relpath(res_file3, pkg_path)
            assert files[1] == os.path.relpath(res_file1, pkg_path)
            assert files[2] == os.path.relpath(res_file2, pkg_path)
        finally:
            if os.path.exists(pkg_path):
                shutil.rmtree(pkg_path)

    def test_invalid_portion(self):
        t, n, m = portion_type_name_modules("invalid_root_path")
        assert not t
        assert n == "{portion-name}"
        assert not m

    def test_no_modules(self):
        root_path = os.path.join(TESTS_FOLDER, TESTS_FOLDER)
        try:
            os.makedirs(root_path)
            with pytest.raises(RuntimeError):
                _t, _n, _m = portion_type_name_modules(root_path)
        finally:
            if os.path.exists(root_path):
                shutil.rmtree(root_path)

    def test_one_module(self):
        t, n, m = portion_type_name_modules(TESTS_FOLDER)
        assert t == 'module'
        assert n == 'test_de_core'
        assert m == ()

    def test_sub_package(self):
        portion_root = os.path.join(TESTS_FOLDER, TESTS_FOLDER)
        package_name = 'tst_sub_pkg'
        package_root = os.path.join(portion_root, package_name)
        tst_file1 = os.path.join(package_root, '__init__.py')
        tst_file2 = os.path.join(package_root, 'module1.py')
        try:
            os.makedirs(package_root)
            with open(tst_file1, 'w') as file_handle:
                file_handle.write("v = 3")
            with open(tst_file2, 'w') as file_handle:
                file_handle.write("v = 3")
            t, n, m = portion_type_name_modules(portion_root)
            assert t == 'sub-package' == PACKAGE_PRJ
            assert n == package_name
            assert m == ('module1', )
        finally:
            if os.path.exists(portion_root):
                shutil.rmtree(portion_root)

    def test_too_much_modules(self):
        tst_file1 = os.path.join(TESTS_FOLDER, 'mod1.py')
        tst_file2 = os.path.join(TESTS_FOLDER, 'mod2.py')
        try:
            with open(tst_file1, 'w') as file_handle:
                file_handle.write("v = 3")
            with open(tst_file2, 'w') as file_handle:
                file_handle.write("v = 55")
            with pytest.raises(RuntimeError):
                _t, _n, _m = portion_type_name_modules(TESTS_FOLDER)
        finally:
            if os.path.exists(tst_file1):
                os.remove(tst_file1)
            if os.path.exists(tst_file2):
                os.remove(tst_file2)
